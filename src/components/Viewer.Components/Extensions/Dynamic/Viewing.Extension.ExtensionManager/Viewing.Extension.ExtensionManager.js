import MultiModelExtensionBase from 'Viewer.MultiModelExtensionBase';
import './Viewing.Extension.ExtensionManager.scss';
import ExtensionPane from './ExtensionPane';
import PaneManager from 'PaneManager';
import sortBy from 'lodash/sortBy';
import React from 'react';

class ExtensionManager extends MultiModelExtensionBase {
  constructor(viewer, options) {
    super(viewer, options);

    this.renderTitle = this.renderTitle.bind(this);
    this.render = this.render.bind(this);

    this.reactOpts = {
      pushRenderExtension: extension => {
        return new Promise(async resolve => {
          const {renderExtensions} = this.react.getState();

          if (!renderExtensions.length) {
            this.react.pushRenderExtension(this);
          }

          this.react.setState({
            renderExtensions: [...renderExtensions, extension],
          });
        });
      },
    };

    this.loadedExtensions = [];
    this.react = options.react;
    this.layout = this.options.layout || {
      flex: 0.35,
    };
  }

  get className() {
    return 'extension-manager';
  }

  // Extension Id
  static get ExtensionId() {
    return 'Viewing.Extension.ExtensionManager';
  }

  // Load callback
  load() {
    const extensions = sortBy(this.options.extensions || [], ext => {
      return ext.loadPriority;
    });

    this.react.setState({
      visible: this.options.visible,
      renderExtensions: [],
      extensions,
    });

    console.log('Viewing.Extension.ExtensionManager loaded');

    return true;
  }

  async loadDependencies() {
    const {extensions} = this.react.getState();
    const loadExts = extensions.filter(extension => {
      return extension.enabled;
    });

    const tasks = loadExts.map(extension => {
      return this.loadDynamicExtension(extension);
    });

    return Promise.all(tasks);
  }

  async loadDynamicExtension(extension) {
    try {
      const {extensions} = this.react.getState();
      extension.loading = true;
      this.react.setState({
        extensions,
      });

      const setState = (state, opts) => {
        const mngState = this.react.getState();
        const extState = mngState[extension.id] || {};

        return this.react.setState(
          {
            [extension.id]: {
              ...extState,
              ...state,
            },
          },
          opts,
        );
      };

      const getState = () => {
        const state = this.react.getState();
        return state[extension.id] || {};
      };

      const options = {
        controlledSelection: true,
        ...this.options,
        ...extension.options,
        react: {
          ...this.react,
          ...this.reactOpts,
          getState,
          setState,
        },
        extensions: null,
        ...this.extMngOpts,
      };

      const loaderFn = !extension.native
        ? this.viewer.loadDynamicExtension
        : this.viewer.loadExtension;

      const extInstance = await loaderFn(extension.id, options);

      extension.loading = false;
      extension.enabled = true;

      this.react.setState({
        extensions,
      });

      this.loadedExtensions.push(extInstance);

      return extInstance;
    } catch (ex) {
      extension.loading = false;
      throw new Error(ex);
    }
  }

  onStopResize() {
    const {renderExtensions} = this.react.getState();
    renderExtensions.forEach(extension => {
      if (extension.onStopResize) {
        extension.onStopResize();
      }
    });
  }

  onResize() {
    const {renderExtensions} = this.react.getState();
    renderExtensions.forEach(extension => {
      if (extension.onResize) {
        extension.onResize();
      }
    });
  }

  renderTitle() {
    return (
      <div className="title">
        <label>Manage Extensions</label>
        <div className="extension-manager controls">
          <button onClick={() => this.showPanel(false)}>
            <span className="fa fa-times" />
          </button>
        </div>
      </div>
    );
  }

  renderExtensions() {
    const {extensions} = this.react.getState();
    const sortedExtensions = sortBy(extensions, ext => ext.displayName);

    return sortedExtensions.map(extension => {
      const className =
        'item' +
        (extension.enabled ? ' enabled' : '') +
        (extension.loading ? ' loading' : '');

      return extension.visible ? (
        <div key={extension.id} className={className}>
          <label>{extension.displayName}</label>
        </div>
      ) : (
        false
      );
    });
  }

  renderExtensionManager = () => {
    return (
      <ExtensionPane
        renderTitle={this.renderTitle}
        key={ExtensionManager.ExtensionId}
        className="extension-manager"
      >
        <div className="extension-list">{this.renderExtensions()}</div>
      </ExtensionPane>
    );
  };

  renderExtTitle(extension) {
    return extension.renderTitle
      ? extension.renderTitle()
      : extension.title || false;
  }

  render() {
    const state = this.react.getState();
    const renderExtensions = sortBy(state.renderExtensions, ext => {
      return ext.options.displayIndex || 0;
    });

    const nbExt = renderExtensions.length;
    const extensionPanes = renderExtensions.map(extension => {
      const flexProp =
        nbExt > 1 && extension.options.flex
          ? {flex: extension.options.flex}
          : {};

      return (
        <ExtensionPane
          renderTitle={() => this.renderExtTitle(extension)}
          onStopResize={e => this.onStopResize()}
          onResize={e => this.onResize()}
          className={extension.className}
          key={extension.id}
          {...flexProp}
        >
          {extension.render({
            showTitle: false,
            docked: true,
          })}
        </ExtensionPane>
      );
    });

    return (
      <div className="extension-manager">
        <PaneManager
          showControls={this.options.showControls}
          orientation="horizontal"
        >
          {extensionPanes}
        </PaneManager>
      </div>
    );
  }
}

Autodesk.Viewing.theExtensionManager.registerExtension(
  ExtensionManager.ExtensionId,
  ExtensionManager,
);
