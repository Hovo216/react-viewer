// Viewing.Extension.DualViewer
// by Philippe Leefsma, April 2016
import MultiModelExtensionBase from 'Viewer.MultiModelExtensionBase';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import WidgetContainer from 'WidgetContainer';
import './Viewing.Extension.DualViewer.scss';
import throttle from 'lodash/throttle';
import Viewer from 'Viewer';
import React from 'react';

class DualViewerExtension extends MultiModelExtensionBase {
  // Class constructor
  constructor(viewer, options) {
    super(viewer, options);

    this.onResize = throttle(this.onResize, 100);
  }

  // Extension Id
  static get ExtensionId() {
    return 'Viewing.Extension.DualViewer';
  }

  // Load callback
  load() {
    this.pathIndex = this.options.defaultPathIndex || 0;

    console.log('Viewing.Extension.DualViewer loaded');

    this.react
      .setState({
        disabled: true,
        activeView: '',
        items: [],
      })
      .then(() => this.react.pushRenderExtension(this));

    return true;
  }

  async getItems() {
    if (this.options.items) return this.options.items;
  }

  async onViewerCreated(viewer) {
    try {
      this.dualViewer = viewer;
      this.viewerModel = null;
      this.dualViewer.start();

      const items = await this.getItems();

      if (items.length) {
        await this.react.setState({disabled: false, items});

        this.setActiveView(items[this.pathIndex]);

        $('#viewer-dropdown')
          .parent()
          .find('ul')
          .css({
            height: Math.min(
              $('.dual-viewer').height() - 42,
              items.length * 26,
            ),
          });
      }
    } catch (ex) {
      console.log('Viewer Initialization Error:');
      console.log(ex);
    }
  }

  renderTitle = () => {
    const state = this.react.getState();

    const menuItems = state.items.map((item, idx) => {
      return (
        <MenuItem
          eventKey={idx}
          key={idx}
          onClick={() => {
            this.setActiveView(item);

            this.pathIndex = idx;
          }}
        >
          {item.name}
        </MenuItem>
      );
    });

    return (
      <div className="title">
        <label>{this.options.title || '2D View'}</label>

        <DropdownButton
          title={'View: ' + state.activeView}
          disabled={state.disabled}
          key={'viewer-dropdown'}
          id="viewer-dropdown"
        >
          {menuItems}
        </DropdownButton>
      </div>
    );
  };

  async setActiveView(item) {
    const path = item.path || this.viewerDocument.getViewablePath(item);

    const options = {
      sharedPropertyDbPath: this.viewerDocument
        ? this.viewerDocument.getPropertyDbPath()
        : null,
    };

    this.dualViewer.loadModel(path, options);
  }

  onStopResize = () => {
    const state = this.react.getState();

    $('#viewer-dropdown')
      .parent()
      .find('ul')
      .css({
        height: Math.min(
          $('.dual-viewer').height() - 42,
          state.items.length * 26 + 16,
        ),
      });
  };

  onResize() {
    if (this.dualViewer && this.dualViewer.impl) {
      this.dualViewer.resize();
    }
  }

  render(opts = {showTitle: true}) {
    return (
      <WidgetContainer showTitle={opts.showTitle} className={this.className}>
        <Viewer
          flex={0.4}
          onViewerCreated={viewer => {
            clearTimeout(this.timeoutId);
            this.timeoutId = setTimeout(
              () => this.onViewerCreated(viewer),
              250,
            );
          }}
          className={this.className}
        />
      </WidgetContainer>
    );
  }
}

Autodesk.Viewing.theExtensionManager.registerExtension(
  DualViewerExtension.ExtensionId,
  DualViewerExtension,
);
