// Viewing.Extension.ContextMenu
// by Philippe Leefsma, September 2016
import MultiModelExtensionBase from 'Viewer.MultiModelExtensionBase';
import sortBy from 'lodash/sortBy';

class ContextMenuExtension extends MultiModelExtensionBase {
  // Class constructor
  constructor(viewer, options) {
    super(viewer, options);

    this.selection = null;
    this.handlers = [];
  }

  // Extension Id
  static get ExtensionId() {
    return 'Viewing.Extension.ContextMenu';
  }

  onBuildMenu = menu => {
    const dbId = this.selection ? this.selection.dbIdArray[0] : null;
    const model = this.selection ? this.selection.model : null;
    const selection = this.selection;

    return this.options.buildMenu ? this.options.buildMenu(menu, dbId) : menu;
  };

  addHandler = handler => this.handlers.push(handler);

  removeHandler = handler => {
    this.handlers = this.handlers.filter(h => h !== handler);
  };

  onSelection(event) {
    this.selection = event.selections.length ? event.selections[0] : null;
  }

  // Unload callback
  unload() {
    const menu = new Autodesk.Viewing.Extensions.ViewerObjectContextMenu(
      this.viewer,
    );

    this.viewer.setContextMenu(menu);

    console.log('Viewing.Extension.ContextMenu unloaded');

    return true;
  }
}

Autodesk.Viewing.theExtensionManager.registerExtension(
  ContextMenuExtension.ExtensionId,
  ContextMenuExtension,
);
