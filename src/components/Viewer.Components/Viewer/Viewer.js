import PropTypes from 'prop-types';
import React from 'react';
import './Viewer.scss';

const Autodesk = window.Autodesk;

class Viewer extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    panels: PropTypes.array,
  };

  static defaultProps = {
    className: '',
    panels: [],
    style: {},
  };

  constructor() {
    super();

    this.loadDynamicExtension = this.loadDynamicExtension.bind(this);
  }

  async loadDynamicExtension(extensionId, options = {}) {
    try {
      console.log(extensionId);
      await import(`../Extensions/Dynamic/${extensionId}/index.js`);

      const extInstance = await this.viewer.loadExtension(extensionId, options);

      if (extInstance.loadDependencies) {
        await extInstance.loadDependencies();
      }

      return extInstance;
    } catch (ex) {
      console.log('Error loading dynamic extension:', extensionId);
      console.log(ex);

      return Promise.resolve(null);
    }
  }

  // Component has been mounted so this container div is now created
  // in the DOM and viewer can be instantiated
  componentDidMount() {
    this.viewer = new Autodesk.Viewing.Private.GuiViewer3D(
      this.viewerContainer,
    );

    this.viewer.loadDynamicExtension = this.loadDynamicExtension;

    this.panelsContainer = document.createElement('div');
    this.viewer.container.appendChild(this.panelsContainer);

    if (this.props.onViewerCreated) {
      this.props.onViewerCreated(this.viewer);
    }
  }

  componentDidUpdate() {
    if (this.viewer && this.viewer.impl) {
      if (
        this.viewerContainer.offsetHeight !== this.height ||
        this.viewerContainer.offsetWidth !== this.width
      ) {
        this.height = this.viewerContainer.offsetHeight;
        this.width = this.viewerContainer.offsetWidth;

        this.viewer.resize();
      }
    }
  }

  // Render component, resize the viewer if exists
  render() {
    const classNames = ['viewer-container', ...this.props.className.split(' ')];

    return (
      <div className="viewer-app-container" style={this.props.style}>
        <div
          ref={div => (this.viewerContainer = div)}
          className={classNames.join(' ')}
          style={this.props.style}
        />
      </div>
    );
  }
}

export default Viewer;
