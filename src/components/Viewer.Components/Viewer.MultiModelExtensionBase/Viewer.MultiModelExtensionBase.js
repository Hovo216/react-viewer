import EventsEmitter from 'EventsEmitter';

export default class MultiModelExtensionBase extends EventsEmitter.Composer(
  Autodesk.Viewing.Extension,
) {
  // Class constructor
  constructor(viewer, options = {}, defaultOptions = {}) {
    super(viewer);

    // bindings

    this.__onLoadNodeProperties = this.__onLoadNodeProperties.bind(this);
    this.__onModelRootLoaded = this.__onModelRootLoaded.bind(this);
    this.__onModelActivated = this.__onModelActivated.bind(this);
    this.__onModelLoaded = this.__onModelLoaded.bind(this);

    this.models = viewer.impl.modelQueue().getModels();

    this.defaultOptions = defaultOptions;

    this.options = {
      ...defaultOptions,
      ...options,
    };

    this.react = this.options.react;

    this.viewer = viewer;

    this.initializeEvents();
  }

  // Extension Id
  static get ExtensionId() {
    return 'Viewing.Extension.MultiModelExtensionBase';
  }

  // Load callback
  load() {
    if (this.options.contextMenu && this.onContextMenu) {
      this.options.contextMenu.addHandler(this.onContextMenu);
    }

    return true;
  }

  // Invoked when extension gets loaded

  // Invoked when the model starts to load
  // The geometry and instanceTree may not be available
  // at this time
  onModelBeginLoad(event) {
    //console.log('MultiModelExtensionBase.onModelBeginLoad')
  }

  // Triggered by ModelLoader extension when a model is
  // selected in a multi-model environment
  onModelActivated(event) {
    //console.log('MultiModelExtensionBase.onModelActivated')
  }

  // Invoked when model root node has been loaded
  // Extensions that do require access to full
  // model geometry or component tree may use that
  // event to know a new model has been loaded
  __onModelRootLoaded(event) {
    this.viewerEvent([
      Autodesk.Viewing.OBJECT_TREE_CREATED_EVENT,
      Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
    ]).then(args => {
      this.onModelCompletedLoad(args[0]);
    });
  }

  // Invoked after onObjectTreeCreated and onGeometryLoaded
  // have both been fired
  onModelCompletedLoad(event) {
    //console.log('MultiModelExtensionBase.onModelCompletedLoad')
  }

  // Invoked once the viewer toolbar has been created
  // Triggered by ModelLoader extension when a model has
  // been unloaded as per user request
  // Invoked when a model is being selected
  //Sink Events
  __onLoadNodeProperties({dbId, properties}) {
    if (this.onLoadNodeProperties) {
      return this.onLoadNodeProperties(dbId, properties);
    }
  }

  __onModelLoaded(event) {
    this.models = [...this.models, event.model];
    this.onModelBeginLoad(event);
  }

  __onModelActivated(event) {
    this.onModelActivated(event);
  }

  // Initialize all events for the extension
  // Each event will invoke a predefined handler
  // implemented or not by the derived extension
  initializeEvents() {
    this.viewerEvents = [];
  }

  emit(eventId, args) {
    return this.options.eventSink
      ? this.options.eventSink.emit(eventId, args)
      : super.emit(eventId, args);
  }

  emitAcc(eventId, args) {
    return this.options.eventSink
      ? this.options.eventSink.emitAcc(eventId, args)
      : super.emitAcc(eventId, args);
  }
}
