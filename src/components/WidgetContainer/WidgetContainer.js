import PropTypes from 'prop-types';
import './WidgetContainer.scss';
import React from 'react';

class WidgetContainer extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    showTitle: PropTypes.bool,
  };

  static defaultProps = {
    showTitle: true,
    className: '',
  };

  renderTitle() {
    return (
      <div className="title">
        <label>{this.props.title}</label>
      </div>
    );
  }

  renderChildren() {
    if (this.props.dimensions) {
      return React.Children.map(this.props.children, child => {
        const newProps = Object.assign({}, child.props, {
          dimensions: this.props.dimensions,
        });

        return React.cloneElement(child, newProps);
      });
    }

    return this.props.children;
  }

  render() {
    const classNames = ['widget-container', ...this.props.className.split(' ')];

    return (
      <div className={classNames.join(' ')} style={this.props.style}>
        <div className="content">{this.renderChildren()}</div>
      </div>
    );
  }
}

export default WidgetContainer;
