import {ReflexElement, ReflexHandle} from 'react-reflex';
import {findDOMNode} from 'react-dom';
import PropTypes from 'prop-types';
import './PaneManager.scss';
import React from 'react';

class PaneElement extends React.Component {
  static propTypes = {
    showTitle: PropTypes.bool,
  };

  static defaultProps = {
    showTitle: true,
  };

  constructor() {
    super();

    this.state = {size: -1};
  }

  renderTitle() {
    if (!this.props.showTitle) return false;

    const {index, events, useHandle} = this.props;

    return useHandle ? (
      <ReflexHandle className="title handle" index={index - 1} events={events}>
        {this.props.renderTitle ? this.props.renderTitle() : this.props.title}
      </ReflexHandle>
    ) : (
      <div className="title">
        {this.props.renderTitle ? this.props.renderTitle() : this.props.title}
      </div>
    );
  }

  render() {
    const style = this.props.showTitle
      ? {height: 'calc(100% - 40px)'}
      : {height: '100%'};

    return (
      <ReflexElement size={this.state.size} {...this.props}>
        <div className="pane-element" style={style}>
          {this.renderTitle()}
          <div className="content">{this.props.children}</div>
        </div>
      </ReflexElement>
    );
  }
}

export default PaneElement;
