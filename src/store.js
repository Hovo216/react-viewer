import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'connected-react-router';
import {createRootReducer, history} from './reducers';
import thunk from 'redux-thunk';

//Services
import ServiceManager from 'SvcManager';
import ModelSvc from 'ModelSvc';

// Middleware Configuration
const middleware = [thunk, routerMiddleware(history)];

// Store Enhancers
const enhancers = [];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function')
    enhancers.push(devToolsExtension());
}

// Store Instantiation and HMR Setup
const initialState = {};

const store = createStore(
  createRootReducer(),
  initialState,
  compose(applyMiddleware(...middleware), ...enhancers),
);

store.asyncReducers = {};

if (module.hot) {
  module.hot.accept('./reducers', () => {
    const reducers = require('./reducers').default;
    store.replaceReducer(reducers(store.asyncReducers));
  });
}

const modelSvc = new ModelSvc({apiUrl: '/api/models'});

ServiceManager.registerService(modelSvc);

export default store;
