import React, {PureComponent} from 'react';
import {ReflexContainer, ReflexElement, ReflexSplitter} from 'react-reflex';

import easing from 'easing-js';
import {merge} from 'lodash';

import EventsEmitter from 'EventsEmitter';
import ServiceManager from 'SvcManager';
import {sleep} from 'Viewer.Toolkit';

import Viewer from 'Viewer';

import './ViewerView.scss';

class ViewerView extends PureComponent {
  constructor(props, context) {
    super(props, context);

    this.modelSvc = ServiceManager.getService('ModelSvc');
    this.eventSink = new EventsEmitter();
    this.state = {
      viewerPanels: [],
      viewerFlex: 1.0,
      resizing: false,
      modelType: '',
    };

    this.viewerFlex = 1.0;
  }

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, () => {
        resolve();
      });
    });
  }

  async componentDidMount() {
    window.addEventListener('resize', this.onStopResize);
    window.addEventListener('resize', this.onResize);

    if (!this.props.appState.viewerEnv) {
      await this.initialize({
        env: 'AutodeskProduction',
        useConsolidation: true,
      });

      this.props.setViewerEnv('AutodeskProduction');

      Autodesk.Viewing.Private.memoryOptimizedSvfLoading = true;
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onStopResize);
    window.removeEventListener('resize', this.onResize);
  }

  initialize(options) {
    return new Promise((resolve, reject) => {
      Autodesk.Viewing.Initializer(
        options,
        () => {
          resolve();
        },
        error => {
          reject(error);
        },
      );
    });
  }

  getViewableItem(doc, {roles, guid}) {}

  buildDefaultOptions(viewer, id) {
    return {
      toolbar: this.toolbar,
      apiUrl: '/api',
      react: {
        pushRenderExtension: this.pushRenderExtension,
        popRenderExtension: this.popRenderExtension,

        getState: () => {
          return this.state[id] || {};
        },
        setState: (state, {doMerge} = {}) => {
          return new Promise(resolve => {
            const extState = this.state[id] || {};

            const newExtState = {
              [id]: doMerge
                ? merge({}, extState, state)
                : {...extState, ...state},
            };

            this.setStateAsync(newExtState).then(() => {
              resolve(newExtState);
            });
          });
        },
        props: this.props,
      },
    };
  }

  loadModel = async (
    viewer,
    {db, id, extIds, urn, path, viewId, linkId},
    delay,
  ) => {
    try {
      let extensions = (extIds || []).map(id => {
        id;
      });
      let dbModel = null;

      if (id) {
        dbModel = await this.modelSvc.getModel(db, id);
      }

      if (dbModel) {
        if (dbModel.extensions) {
          extensions = [...extensions, ...dbModel.extensions];
        }

        if (dbModel.model.path) {
          path = dbModel.model.path;
        }

        viewer.stateInit = dbModel.stateInit;
      } else if (!path) {
        const error =
          'Invalid query parameter: ' +
          'use id OR linkId OR urn OR path in url';

        throw new Error(error);
      }

      viewer.start();

      viewer.addEventListener(
        Autodesk.Viewing.MODEL_ROOT_LOADED_EVENT,
        this.onModelRootLoaded,
      );
      viewer.addEventListener(
        Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
        this.onGeometryLoaded,
      );

      if (delay) await sleep(delay);

      const tasks = extensions.map(extension => {
        const extId = extension.id;
        const options = {
          ...this.buildDefaultOptions(viewer, extId),
          ...extension.options,
        };

        return viewer.loadDynamicExtension(extension.id, options);
      });

      await Promise.all(tasks);

      viewer.loadModel(path, model => {
        this.eventSink.emit('model.activated', {
          source: 'model.loaded',
          model,
        });

        this.eventSink.emit('model.loaded', {
          model,
        });
      });
    } catch (ex) {
      this.handleError(ex);
    }
  };

  onViewerCreated(viewer) {
    viewer.addEventListener(Autodesk.Viewing.EXTENSION_LOADED_EVENT);

    const {search} = this.props.location;
    const id = search.split('=')[1];

    this.loadModel(viewer, {id});
  }

  handleError = error => {
    console.log('Viewer Initialization Error: ', error);

    switch (error.status) {
      case 404:
        break;
    }
  };

  onStopResize = e => {
    const keys = ['rightPane', 'leftPane', 'topPane'];

    keys.forEach(key => {
      if (this.state[key]) {
        if (this.state[key].onStopResize) {
          this.state[key].onStopResize();
        }
      }
    });
  };

  onResize = event => {
    const keys = ['rightPane', 'leftPane', 'topPane'];

    keys.forEach(key => {
      if (this.state[key]) {
        if (this.state[key].onResize) {
          this.state[key].onResize();
        }
      }
    });
  };

  onViewerStartResize = e => this.setState({resizing: true});

  onViewerStopResize = e => {
    this.viewerFlex = e.component.props.flex;

    const keys = ['rightPane', 'leftPane', 'topPane'];

    keys.forEach(key => {
      if (this.state[key]) {
        if (this.state[key].onStopResize) {
          this.state[key].onStopResize();
        }
      }
    });

    this.setState({resizing: false});
  };

  onPaneStopResize(flexKey, e) {
    this[flexKey] = e.component.props.flex;
  }

  pushRenderExtension = async extension => {
    const layout = extension.layout;
    const location = layout.location || 'rightPane';

    this[`${location}Flex`] = layout.flex;

    if (['leftPane', 'rightPane'].includes(location)) {
      this.viewerFlex = layout ? 1.0 - layout.flex : 1.0;

      const paneExtStyle = {
        display: 'block',
      };

      await this.setStateAsync({
        [`${location}Flex`]: layout.flex,
        paneExtStyle,
      });

      await this.runAnimation(1.0, this.viewerFlex, 1.0);
    }

    await sleep(250);

    await this.setStateAsync({
      [location]: extension,
    });
  };

  popRenderExtension = async extension => {
    const layout = extension.layout;
    const location = layout.location || 'rightPane';

    this[`${location}Flex`] = 1.0;

    await this.setStateAsync({
      [location]: null,
    });

    await sleep(250);

    if (['leftPane', 'rightPane'].includes(location)) {
      await this.runAnimation(this.viewerFlex, 1.0, 1.0);

      const paneExtStyle = {
        display: 'none',
      };

      await this.setStateAsync({
        [`${location}Flex`]: 1.0,
        paneExtStyle,
      });
    }
  };

  onModelRootLoaded = event => {
    const viewer = event.target;
    viewer.removeEventListener(
      Autodesk.Viewing.MODEL_ROOT_LOADED_EVENT,
      this.onModelRootLoaded,
    );

    if (viewer.model.is2d()) {
      this.setStateAsync({
        modelType: 'model2d',
      });
    }

    if (viewer.model.is3d()) {
      this.setStateAsync({
        modelType: 'model3d',
      });
    }
  };

  onGeometryLoaded = event => {
    const viewer = event.target;
    viewer.removeEventListener(
      Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
      this.onGeometryLoaded,
    );

    if (viewer.model.is3d()) {
      const nav = viewer.navigation;

      if (viewer.stateInit) {
        viewer.restoreState(viewer.stateInit, null, true);
      } else {
        nav.toPerspective();
      }

      viewer.autocam.setHomeViewFrom(nav.getCamera());

      if (viewer.model.transform) {
        transformModel(viewer, viewer.model, viewer.model.transform);
      }
    }

    setTimeout(() => {
      if (viewer.viewCubeUi) {
        viewer.showViewCubeTriad(true);
      }
    }, 2000);
  };

  animate(period, easing, update) {
    return new Promise(resolve => {
      let elapsed = 0;
      const stepFn = () => {
        elapsed += 1;

        if (elapsed < period) {
          const eased = easing(elapsed / period);
          update(eased).then(() => {
            window.requestAnimationFrame(stepFn);
          });
        } else {
          update(1.0);
          resolve();
        }
      };
      stepFn();
    });
  }

  runAnimation(start, end, animPeriod) {
    const easingFn = t => {
      //b: begging value, c: change in value, d: duration
      return easing.easeInOutExpo(t, 0, 1.0, animPeriod * 0.9);
    };

    const update = eased => {
      const viewerFlex = (1.0 - eased) * start + eased * end;

      return new Promise(resolve => {
        this.setStateAsync({
          viewerFlex,
        }).then(() => resolve());
      });
    };

    return this.animate(animPeriod, easingFn, update);
  }

  renderModel() {
    const {resizing, viewerPanels, modelType} = this.state;
    const viewerStyle = {
      pointerEvents: resizing ? 'none' : 'all',
    };

    return (
      <Viewer
        className={modelType}
        onViewerCreated={viewer => {
          this.onViewerCreated(viewer);
        }}
        panels={viewerPanels}
        style={viewerStyle}
      />
    );
  }

  renderExtension(extension) {
    const renderOptions = {
      showTitle: true,
      docked: true,
    };

    return (
      <div className="data-pane">
        {extension && extension.render(renderOptions)}
      </div>
    );
  }

  render() {
    const navbar = this.props.appState.navbar.visible ? 'with-navbar' : '';
    const mobile = this.props.appState.isMobile;
    const {
      rightPaneFlex,
      leftPaneFlex,
      paneExtStyle,
      viewerFlex,
      rightPane,
      leftPane,
      topPane,
    } = this.state;

    return (
      <div className={`viewer-view ${navbar} ${mobile ? 'mobile' : ''}`}>
        {topPane && (
          <div {...topPane.layout.style}>{this.renderExtension(topPane)}</div>
        )}
        <ReflexContainer orientation="horizontal" key="viewer">
          <ReflexElement>
            <ReflexContainer orientation="vertical">
              {leftPaneFlex && (
                <ReflexSplitter
                  onStopResize={() => this.forceUpdate()}
                  style={paneExtStyle}
                />
              )}
              {leftPane && (
                <ReflexElement
                  onStopResize={e => this.onPaneStopResize('leftPaneFlex', e)}
                  style={paneExtStyle}
                >
                  {this.renderExtension(leftPane)}
                </ReflexElement>
              )}
              <ReflexElement
                onStartResize={this.onViewerStartResize}
                onStopResize={this.onViewerStopResize}
                propagateDimensions={true}
                onResize={this.onResize}
                className="viewer"
                flex={viewerFlex}
              >
                {this.renderModel()}
              </ReflexElement>
              {rightPaneFlex && (
                <ReflexSplitter
                  onStopResize={() => this.forceUpdate()}
                  style={paneExtStyle}
                />
              )}
              {rightPaneFlex && (
                <ReflexElement
                  onStopResize={e => this.onPaneStopResize('righPaneFlex', e)}
                  style={paneExtStyle}
                >
                  {this.renderExtension(rightPane)}
                </ReflexElement>
              )}
            </ReflexContainer>
          </ReflexElement>
        </ReflexContainer>
      </div>
    );
  }
}

export default ViewerView;
