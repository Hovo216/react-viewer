import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import React from 'react';
import './Home.scss';

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

const mapStateToProps = state => ({});

const Home = props => (
  <div className="home">
    <div className="title">
      <Link className="app-link" to="/viewer?id=2">
        Extensions Manager
      </Link>
    </div>
  </div>
);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
