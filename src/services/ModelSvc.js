// import BaseSvc from './BaseSvc';
import EventsEmitter from 'EventsEmitter';

// export default class ModelSvc extends BaseSvc {
export default class ModelSvc extends EventsEmitter {
  constructor(opts) {
    super(opts);
  }

  get name() {
    return 'ModelSvc';
  }

  getModel(dbName, modelId) {
    let publicUrl = process.env.PUBLIC_URL;

    const models = {
      2: {
        model: {
          path: `${publicUrl}/models/office/Resource/3D_View/3D/model.svf`,
        },
        extensions: [
          {
            id: 'Viewing.Extension.ExtensionManager',
            options: {
              useStorage: true,
              visible: true,
              extensions: [
                {
                  id: 'Viewing.Extension.DualViewer',
                  displayName: 'Dual Viewer',
                  enabled: true,
                  visible: true,
                  options: {
                    items: [
                      {
                        name: 'Basement & Ground Floor',
                        path: `${publicUrl}/models/office/4df86081-3d3b-4c35-fbdd-234af5d7f5c7_f2d/primaryGraphics.f2d`,
                      },
                      {
                        name: 'Plumbing Details',
                        path: `${publicUrl}/models/office/93f583fd-001d-6fb0-13ac-4301b0fc02b7_f2d/primaryGraphics.f2d`,
                      },
                      {
                        name: 'Terrace & Roof',
                        path: `${publicUrl}/models/office/062bdb73-fda2-4b97-76e2-4127142a26c3_f2d/primaryGraphics.f2d`,
                      },
                      {
                        name: '1st & 2nd Floor',
                        path: `${publicUrl}/models/office/020360db-5f21-d5f0-2d01-af91604871d5_f2d/primaryGraphics.f2d`,
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    };

    return Promise.resolve(models[modelId]);
  }
}
